#! /usr/bin/env node

// Here is where to write the ES6 Code && MAKE SURE ^^^THAT LINE MAKES IT TO app.js AFTER TRANSPILING

"use strict";

var fs = require("fs");
var shell = require("shelljs");

var filename = "package.json";
var projname = "";
var semversion = "0.0.1";
function doGitStuff() {
    console.log("doing git stuff for " + projname + " version: " + semversion);
    shell.exec("git tag -a v" + semversion + " -m \"SemVerUpdated git tag\" && git commit -am \"auto update with SemVerUpdater\" && git push --tags");
    console.log("finished pushing tags to git");
}
function updateData(data) {
    data = JSON.parse(data);
    console.log(data.name + " version before:: " + data.version);
    var firstNums = data.version.toString().split(".");
    firstNums[2] = +firstNums[2] + 1;
    data.version = firstNums[0] + "." + firstNums[1] + "." + firstNums[2];
    console.log(data.name + " version after:: " + data.version);
    semversion = data.version;
    return data;
}
function saveData(data) {
    var dataname = data.name;
    data = JSON.stringify(data, null, 4);
    fs.writeFile(filename, data, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log(dataname + " saved " + filename + " with an updated version");
        doGitStuff();
    });
}
fs.exists(filename, function (exists) {
    if (exists) {
        fs.stat(filename, function (error, stats) {
            fs.open(filename, "r", function (error, fd) {
                var buffer = new Buffer(stats.size);

                fs.read(fd, buffer, 0, buffer.length, null, function (error, bytesRead, buffer) {
                    var data = buffer.toString("utf8", 0, buffer.length);
                    fs.close(fd);
                    saveData(updateData(data));
                });
            });
        });
    }
});
